FROM python:2.7.18
MAINTAINER dengybouo@gmail.com
RUN mkdir /app
WORKDIR /app
ADD sphinxapi.py /app
ADD coreseek.py /app
ADD requirements.txt /app
RUN chmod +x /app/coreseek.py
RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt
EXPOSE 7003
CMD ["python","coreseek.py"]
