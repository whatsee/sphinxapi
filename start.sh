docker stop pysphinxapi
docker rm pysphinxapi
docker run -d \
--name pysphinxapi \
--restart=always \
-p 7003:7003 \
-t deng/pysphinxapi:v1 \
python /app/coreseek.py \
--ck_ip=10.0.0.171 \
--ck_port=9314
