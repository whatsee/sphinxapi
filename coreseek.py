# -*- coding: utf-8 -*-
# python2
from  gevent.pywsgi import WSGIServer
from gevent import monkey
monkey.patch_all()
from sphinxapi import *
import time, json
from flask import Flask, request
import sys
import argparse

__version__ = 'v1.1.2'

parser = argparse.ArgumentParser()
parser.add_argument('--ck_ip', type=str, default="10.0.0.171", help='coreseek ip')
parser.add_argument('--ck_port', type=int, default=9314, help='coreseek port')
parser.add_argument('--host_port', type=str, default=7003, help='server port')
args = parser.parse_args()

app = Flask(__name__)

sc = SphinxClient()
sc.SetServer ( args.ck_ip, args.ck_port )

@app.route('/search',methods=['POST','GET'])
def search():
    kw = request.form.get('keyword')
    kw_num = request.form.get('number')
    kw_weights = request.form.get('weights')
    kw_index = request.form.get('index')
    match_model = request.form.get('match_model')
    kw_select = request.form.get('select')

    sc.SetLimits(0, int(kw_num)) 
    sc.SetWeights ([ int(kw_weights), 1])
    sc.SetMatchMode ( int(match_model) )
    sc.SetSelect( str(kw_select) )
    res = sc.Query(kw, kw_index.encode('utf-8')) #前一个是关键字，后一个是索引，*代表所有索引
    res = json.dumps(res)
    return res

if __name__ == '__main__':
    #app.run(host='0.0.0.0',port=args.host_port,debug=False)
    print("begin to start server")
    http_server = WSGIServer(('0.0.0.0', int(args.host_port)), app)
    http_server.serve_forever()
